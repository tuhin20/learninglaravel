@extends('layouts.admin')
@section('title')
    Manage Category
@endsection
@section('content')
    <div class="col-lg-8 offset-lg-2">
        <div class="card-box">
            <h4 class="header-title" style="text-align: center;">Category List</h4>

            <?php
            $message=Session::get('message');
            if($message){

            ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                echo $message;
                Session::put('message','');
                ?>
            </div>
            <?php

            }
            ?>

            <div class="table-responsive">
                <table class="table table-striped mb-0">
                    <thead>
                    <tr>

                        <th>Name</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($categoryInfo as $category)

                    <tr>
                       <td>{{$category->categoryName}}</td>
                       <td>{{$category->categoryDescription}}</td>
                       <td>
                           <?php
                                if($category->publicationStatus==1){
                                    echo "Published";
                                }else{
                                    echo "Unpublished";
                                }

                           ?>
                       </td>
                       <td>

                           <a href="{{route('editCategory',[$category->categoryId,$categoryInfo->currentPage()])}}"  class="btn btn-warning">Edit</a>
                           <a href="{{route('deleteCategory',[$category->categoryId,$categoryInfo->currentPage()])}}" onclick=" return confirm('Are You Sure?')"  class="btn btn-danger">Remove</a>
                       </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div> <!-- end table-responsive-->
            {{$categoryInfo->links()}}
        </div> <!-- end card-box -->
    </div> <!-- end col -->
    @endsection