@extends('layouts.admin')
@section('title')
    Edit Category
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <div class="card-box">
                <h4 class="header-title m-t-0" style="text-align: center;">Edit Category</h4>


                <form action="{{route('updateCategory')}}" method="post" class="parsley-examples">
                    @csrf



                    @if($errors->any())

                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">


                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>


                        </div>
                    @endif
                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="hidden" name="categoryId" value="{{$categoryId}}" />
                        <input type="hidden" name="page" value="{{$page}}" />
                        <input type="text" value="{{$singleCategoryInfo->categoryName}}" name="categoryName" class="form-control" required
                               data-parsley-required-message="Please enter category name" placeholder="Category Name"/>
                    </div>
                    <div class="form-group">
                        <label>Category Description</label>
                        <textarea rows="6" class="form-control" name="categoryDescription" placeholder="Category Description">{{$singleCategoryInfo->categoryDescription}}</textarea>
                    </div>

                    <div class="form-group">
                        <label>Publication Status</label>
                        <select class="form-control" name="publicationStatus" required  data-parsley-required-message="Please Select Publication Status" >
                            <option value="">---Select Publication Status---</option>
                            <?php
                                if($singleCategoryInfo->publicationStatus==1){


                            ?>
                                <option value="1" selected>Published</option>
                                <option value="0">Unpublished</option>
                            <?php } else{?>
                                <option value="1" >Published</option>
                                <option value="0" selected >Unpublished</option>
                            <?php }?>
                        </select>
                    </div>


                    <div class="form-group mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                Update
                            </button>

                        </div>
                    </div>
                </form>
            </div> <!-- end card-box -->
        </div> <!-- end col-->


    </div>
    <!-- end row -->
@endsection