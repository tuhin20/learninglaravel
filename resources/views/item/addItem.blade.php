@extends('layouts.admin')
@section('title')
    Add Item Please
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <div class="card-box">
                <h4 class="header-title m-t-0" style="text-align: center;">Add Item</h4>


                <form action="{{route('saveManufacturer')}}" method="post" class="parsley-examples">
                    @csrf

                    <?php
                    $message=Session::get('message');
                    if($message){

                    ?>
                    <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                        echo $message;
                        Session::put('message','');
                        ?>
                    </div>
                    <?php

                    }
                    ?>

                    <?php
                    $warningMessage=Session::get('warningMessage');
                    if($warningMessage){

                    ?>
                    <div class="alert alert-warning alert-dismissible bg-warning text-white border-0 fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php
                        echo $warningMessage;
                        Session::put('warningMessage','');
                        ?>
                    </div>
                    <?php

                    }
                    ?>

                    @if($errors->any())

                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">


                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>


                        </div>
                    @endif
                     <div class="form-group">
                        <label>Manufacturer Name</label>
                        <input type="text" rows="6" class="form-control"  required
                               data-parsley-required-message="Please enter Manufacturer name" name="manufacturerName" placeholder="Manufacturer Name">
                    </div>
                
                    <div class="form-group">
                        <label>Manufacturer Description</label>
                        <input type="text" rows="6" class="form-control"  required
                               data-parsley-required-message="Please enter manufacturer description" name="manufacturerDescription" placeholder="Manufacturer Description">
                    </div>
                     

                    <div class="form-group">
                        <label>Publication Status</label>
                        <select class="form-control" name="publicationStatus" required  data-parsley-required-message="Please Select Publication Status" >
                            <option value="">---Select Publication Status---</option>
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div>


                    <div class="form-group mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                Submit
                            </button>

                        </div>
                    </div>
                </form>
            </div> <!-- end card-box -->
        </div> <!-- end col-->


    </div>
    <!-- end row -->
@endsection