@extends('layouts.admin')
@section('title')
    Edit Product
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            <div class="card-box">
                <h4 class="header-title m-t-0" style="text-align: center;">Edit Product</h4>


                <form action="{{route('updateProduct')}}" enctype="multipart/form-data" method="post" class="parsley-examples">
                    @csrf



                    @if($errors->any())

                        <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">


                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>


                        </div>
                    @endif
                    <div class="form-group">
                        <label>Product Image</label>
                        <br/>
                        <img class="rounded-circle avatar-md" src="{{asset($singleProductInfo->productImage)}}">
                        <input type="hidden" name="productId" value="{{$productId}}" />
                        <input type="hidden" name="page" value="{{$page}}" />
                        <input type="file"  name="productImage" class="form-control"
                                placeholder="Product Image"/>
                    </div>
                    <div class="form-group">
                        <label>Product Name</label>
                        <input type="text" rows="6" class="form-control" name="productName" placeholder="Product Name" value="{{$singleProductInfo->productName}}"></input>
                    </div>
                     
                     <div class="form-group">
                        <label>Product Description</label>
                        <input type="text" rows="6" class="form-control" name="productDescription" placeholder="Product Description" value="{{$singleProductInfo->productDescription}}"></input>
                    </div>
                    <div class="form-group">
                        <label>Product Price</label>
                        <input type="text" rows="6" class="form-control" name="productPrice" placeholder="Product Price" value="{{$singleProductInfo->productPrice}}"></input>
                    </div>

                    <div class="form-group">
                        <label>Publication Status</label>
                        <select class="form-control" name="publicationStatus" required  data-parsley-required-message="Please Select Publication Status" >
                            <option value="">---Select Publication Status---</option>
                            <?php
                            if($singleProductInfo->publicationStatus==1){


                            ?>
                            <option value="1" selected>Published</option>
                            <option value="0">Unpublished</option>
                            <?php } else{?>
                            <option value="1" >Published</option>
                            <option value="0" selected >Unpublished</option>
                            <?php }?>
                        </select>
                    </div>


                    <div class="form-group mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                Update
                            </button>

                        </div>
                    </div>
                </form>
            </div> <!-- end card-box -->
        </div> <!-- end col-->


    </div>
    <!-- end row -->
@endsection