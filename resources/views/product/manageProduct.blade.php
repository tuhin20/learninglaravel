@extends('layouts.admin')
@section('title')
    Manage Product
@endsection
@section('content')
    <div class="col-lg-10 offset-lg-1">
        <div class="card-box">
            <h4 class="header-title" style="text-align: center;">Product List</h4>

            <?php
            $message=Session::get('message');
            if($message){

            ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                echo $message;
                Session::put('message','');
                ?>
            </div>
            <?php
            }
            ?>

            <div class="table-responsive">
                <table class="table table-striped mb-0">
                    <thead>
                    <tr>

                        <th>Product Image</th>
                        <th>Product Name</th>
                        <th>Product Price</th>
                        <th>Product Description</th>
                        <th>Category Name</th>
                        <th>Status</th>
                        <th>Action</th>
                        
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($productInfo as $product)

                        <tr>
                            <td><img class="rounded-circle avatar-sm" src="{{asset($product->productImage)}}" title="{{$product->productImage}}"></td>
                            <td>{{$product->productName}}</td>
                            <td>{{$product->productPrice}}</td>
                            <td>{{$product->productDescription}}</td>
                            <td>{{$product->categoryName}}</td>
                            <td>
                                <?php
                                if($product->publicationStatus==1){
                                    echo "Published";
                                }else{
                                    echo "Unpublished";
                                }

                                ?>
                            </td>
                            <td>

                                <a href="{{route('editProduct',[$product->productId,$productInfo->currentPage()])}}" class="btn btn-warning">Edit</a> <a href="{{route('deleteProduct',[$product->productId,$productInfo->currentPage()])}}" onclick=" return confirm('Are You Sure?')"  class="btn btn-danger">Remove</a>
                               
                           </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div> <!-- end table-responsive-->
            {{$productInfo->links()}}
        </div> <!-- end card-box -->
    </div> <!-- end col -->
@endsection

