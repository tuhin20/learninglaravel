@extends('layouts.admin')
@section('title')
    Manage Manufacturer
@endsection
@section('content')
    <div class="col-lg-10 offset-lg-1">
        <div class="card-box">
            <h4 class="header-title" style="text-align: center;">Manufacturer List</h4>

            <?php
            $message=Session::get('message');
            if($message){

            ?>
            <div class="alert alert-success alert-dismissible bg-success text-white border-0 fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php
                echo $message;
                Session::put('message','');
                ?>
            </div>
            <?php
            }
            ?>

            <div class="table-responsive">
                <table class="table table-striped mb-0">
                    <thead>
                    <tr>

                        <th>Manufacturer Name</th>
                        <th>Manufacturer Description</th>
                        <th>Product Name</th>
                        <th>Status</th>
                        <th>Action</th>
                        
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($manuproductInfo as $manufacturer)

                        <tr>
                            
                            <td>{{$manufacturer->manufacturerName}}</td>
                            <td>{{$manufacturer->manufacturerDescription}}</td>
                            <td>{{$manufacturer->productName}}</td>
                            <td>
                                <?php
                                if($manufacturer->publicationStatus==1){
                                    echo "Published";
                                }else{
                                    echo "Unpublished";
                                }
                              
                                ?>
                            </td>
                            <td>

                                <a href="{{route('editManufacturer',[$manufacturer->manufacturerId,$manuproductInfo->currentPage()])}}"  class="btn btn-warning">Edit</a>
                               
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div> <!-- end table-responsive-->
            {{$manuproductInfo->links()}}
        </div> <!-- end card-box -->
    </div> <!-- end col -->
@endsection

