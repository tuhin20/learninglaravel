<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class productRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'productName'=>'required',
            'productPrice'=>'required',
            'publicationStatus'=>'required',
            'productDescription'=>'required',
             //'categoryId'=>'required',
            
        ];
    }
    public function messages()
    {
        return [

            'productName.required' => 'Please Enter Product Name',
            'productPrice.required' => 'Please Enter Product Price',
            'publicationStatus.required'  => 'Please Select Publication Status',
              'productDescription.required'=>'Please Select productDescription',
             // 'categoryId.required'=>'Please Select Category Name',
        ];
    }
}
