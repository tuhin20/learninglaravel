<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class manufacturerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

         
            'manufacturerName'=>'required',
            'manufacturerDescription'=>'required',
            'publicationStatus'=>'required',
           
        ];
    }
    public function messages()
    {
        return [
            
            'manufacturerName.required' => 'Please Enter manufacturer Name',
            'manufacturerDescription.required' => 'Please Enter manufacturer Description',
            'publicationStatus.required'  => 'Please Select Publication Status',

        ];
    }
}
