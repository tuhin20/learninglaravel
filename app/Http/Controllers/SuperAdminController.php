<?php

namespace App\Http\Controllers;

use App\Http\Requests\productRequest;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\manufacturerRequest;

use DB;
use Session;
use File;

if (!isset($_SESSION)){
    session_start();
}
date_default_timezone_set('Asia/Dhaka');

class SuperAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('loginCheck');
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function logout()
    {
        Session::put('id', '');
        Session::put('name', '');
        return redirect()->route('adminLogin');
    }

    /* Category Start*/
    public function addCategory()
    {
        return view('category.addCategory');
        //return view('category.manageCategory');
    }

    public function saveCategory(CategoryRequest $request)
    {
        $data = array();
        $data['categoryName'] = $request->categoryName;
        $data['categoryDescription'] = $request->categoryDescription;
        $data['publicationStatus'] = $request->publicationStatus;
        $data['created_at'] = date('Y-m-d G:i:s');
        $data['updated_at'] = date('Y-m-d G:i:s');

        DB::table('category')
            ->insert($data);

        Session::put('message', 'Category Save Successfully');

        return redirect()->back();
    }

    public function manageCategory()
    {

        $categoryInfo = DB::table('category')
            ->orderby('categoryId', 'desc')
            ->paginate(2);
        return view('category.manageCategory', compact('categoryInfo'));
    }

    public function editCategory($categoryId, $page)
    {
        $singleCategoryInfo = DB::table('category')
            ->where('categoryId', $categoryId)
            ->first();

        return view('category.editCategory', compact('singleCategoryInfo', 'categoryId', 'page'));
    }

    public function updateCategory(CategoryRequest $request)
    {
        $categoryId = $request->categoryId;
        $page = $request->page;
        $data = array();
        $data['categoryName'] = $request->categoryName;
        $data['categoryDescription'] = $request->categoryDescription;
        $data['publicationStatus'] = $request->publicationStatus;

        $data['updated_at'] = date('Y-m-d G:i:s');

        DB::table('category')
            ->where('categoryId', $categoryId)
            ->update($data);

        Session::put('message', 'Category Update Successfully');

        return redirect('admin/manage-category?page=' . $page);
    }

    public function deleteCategory($categoryId, $page)
    {

        DB::table('category')
            ->where('categoryId', $categoryId)
            ->delete();

        Session::put('message', 'Category Delete Successfully');

        return redirect('admin/manage-category?page=' . $page);
    }

    /* Category end*/
    public function addProduct()
    {
        $categoryInfo=DB::table('category')
                        ->get();
        return view('product.addProduct',compact('categoryInfo'));
    }
    public function saveProduct(productRequest $request){
        $data = array();

        $data['productName'] = $request->productName;
        $data['productPrice'] = $request->productPrice;
        $data['productDescription'] = $request->productDescription;
        $data['categoryId'] = $request->categoryId;
        $data['publicationStatus'] = $request->publicationStatus;
        $data['created_at'] = date('Y-m-d G:i:s');
        $data['updated_at'] = date('Y-m-d G:i:s');
        $image=$request->file('productImage');

        if ($image) {
            $image_name=str_random(20);
            $ext=strtolower($image->getClientOriginalExtension());

            $permited  = array('jpg', 'jpeg', 'png');
            $fileSize = $request->file('productImage')->getClientSize();

            if($fileSize>524288){
                Session::put('warningMessage','Please upload file less than 512 KB');
                return redirect()->back();
            }elseif (in_array($ext, $permited) === false){

                Session::put('warningMessage','You can only upload '.implode(', ', $permited));
                return redirect()->back();
            }


            $image_full_name=$image_name.'.'.$ext;
            $upload_path='admin/img/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            if ($success) {



                $data['productImage']=$image_url;
                DB::table('addProduct')
                    ->insert($data);

                Session::put('message','Product Save successfully!!');
                return redirect()->back();
            }
        }

        $data['productImage']="";
            DB::table('addProduct')
            ->insert($data);

        Session::put('message', 'Product Save Successfully');

        return redirect()->back();
    }
    public function manageProduct()
    {

        $productInfo = DB::table('addProduct as p')
            ->join('category as c','c.categoryId','=','p.categoryId') //understanding
            ->select('p.*','c.categoryName')
            ->orderby('p.categoryId', 'desc')
            ->paginate(2);
        return view('product.manageProduct', compact('productInfo'));
    }
    public function editProduct($productId, $page)
    {
        $singleProductInfo = DB::table('addProduct')
            ->where('productId', $productId)
            ->first();

        return view('product.editProduct', compact('singleProductInfo', 'productId', 'page'));
    }
    public function updateProduct(productRequest $request)
    {
        $productId = $request->productId;
        $page = $request->page;
        $productInfo=DB::table('addProduct')
                            ->where('productId',$productId)
                            ->select('productImage')
                            ->first();
        $data = array();
        $data['productName'] = $request->productName;
          $data['productDescription'] = $request->productDescription;
        $data['productPrice'] = $request->productPrice;
        $data['publicationStatus'] = $request->publicationStatus;

        $data['updated_at'] = date('Y-m-d G:i:s');


        $image=$request->file('productImage');

        if ($image) {
            $image_name=str_random(20);
            $ext=strtolower($image->getClientOriginalExtension());

            $permited  = array('jpg', 'jpeg', 'png');
            $fileSize = $request->file('productImage')->getClientSize();

            if($fileSize>524288){
                Session::put('warningMessage','Please upload file less than 512 KB');
                return redirect()->back();
            }elseif (in_array($ext, $permited) === false){

                Session::put('warningMessage','You can only upload '.implode(', ', $permited));
                return redirect()->back();
            }


            $image_full_name=$image_name.'.'.$ext;
            $upload_path='admin/img/';
            $image_url=$upload_path.$image_full_name;
            $success=$image->move($upload_path,$image_full_name);
            if ($success) {

                //Delete Previous Image start
                File::delete(public_path($productInfo->productImage));
                //Delete Previous Image end

                $data['productImage']=$image_url;
                DB::table('addProduct')
                    ->where('productId', $productId)
                    ->update($data);

                Session::put('message','Product Update successfully!!');
                return redirect()->back();
            }
        }


        DB::table('addProduct')
            ->where('productId', $productId)
            ->update($data);

        Session::put('message', 'Product Update Successfully');

        return redirect('admin/manage-product?page=' . $page);
    }
    public function deleteProduct($productId, $page)
    {

        DB::table('addProduct')
            ->where('productId', $productId)
            ->delete();

        Session::put('message', 'Product Delete Successfully');

        return redirect('admin/manage-product?page=' . $page);
    }
    public function addManufacturer(){
         $productInfo=DB::table('addProduct')
                        ->get();
        return view('manufacturer.add-manufacturer',compact('productInfo'));

    }
    public function saveManufacturer(manufacturerRequest $request){
    $data=array();
    
    $data['manufacturerName']=$request->manufacturerName;
    $data['manufacturerDescription']=$request->manufacturerDescription;
    $data['publicationStatus'] = $request->publicationStatus;
     $data['productId'] = $request->productId;
  
      DB::table('addmanufacturer')
                    ->insert($data);
      Session::put('message','manufacturer Save successfully!!');
                return redirect()->back();
    }
    public function manageManufacturer()
    {
          $manuproductInfo = DB::table('addManufacturer as m')
            ->join('addProduct as p','m.productId','=','p.productId')
            ->select('m.*','p.productName')
            ->orderby('m.productId', 'desc')
            ->paginate(2);

            // echo '<pre/>';
            // print_r($manuproductInfo);
            // exit();
        return view('manufacturer.manageManufacturer', compact('manuproductInfo'));
    }
    public function editManufacturer($id, $page){

         $singleManufacturerInfo = DB::table('addManufacturer')
            ->where('id', $id)
            ->first();

        return view('manufacturer.editManufacturer', compact('singleManufacturerInfo', 'id', 'page'));
    }
}
