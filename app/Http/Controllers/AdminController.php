<?php

namespace App\Http\Controllers;
use App\Http\Requests\LoginRequest;

use Illuminate\Http\Request;
use DB;
use Session;


if (!isset($_SESSION)){
    session_start();
}

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('login');
    }

    public  function login(){
        return view('admin.login');

    }
    public function signIn(LoginRequest $request){
        //dd($request);
        $email=$request->email;
        $password=md5($request->password);
        $result=DB::table('admin')
            ->where('email',$email)
            ->where('password',$password)
            ->first();
        if($result){
          Session::put('name',$result->name);
          Session::put('id',$result->id);
             return redirect()->route('adminDashboard');
        }
        else{
            Session::put("message","Your email and password Invalid");

            return redirect()->back();
        }
    }
}
