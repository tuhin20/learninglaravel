<?php

namespace App\Http\Middleware;

use Closure;
use Session;
if (!isset($_SESSION)){
    session_start();
}
class LoginCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id=Session::get('id');
        if (!$id){
            return redirect()->route('adminLogin');
        }
        return $next($request);
    }
}
