<?php

namespace App\Http\Middleware;

use Closure;
use Session;


if (!isset($_SESSION)){
    session_start();
}

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id=Session::get('id');
        if ($id){
            return redirect()->route('adminDashboard');
        }
        return $next($request);
    }
}
