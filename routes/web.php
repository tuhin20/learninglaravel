<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::prefix('admin')->group(function (){
   Route::get('login','AdminController@login')->name('adminLogin');
   Route::post('signIn','AdminController@signIn')->name('signIn');
   Route::get('dashboard','SuperAdminController@dashboard')->name('adminDashboard');
   Route::get('logout','SuperAdminController@logout')->name('superAdminLogout');
    /* Category Start*/
   Route::get('add-category','SuperAdminController@addCategory')->name('addCategory');
   Route::post('save-category','SuperAdminController@saveCategory')->name('saveCategory');
   Route::get('manage-category','SuperAdminController@manageCategory')->name('manageCategory');
   Route::get('edit-category/{id}/{page}','SuperAdminController@editCategory')->name('editCategory');
   Route::post('update-category','SuperAdminController@updateCategory')->name('updateCategory');
   Route::get('delete-category/{id}/{page}','SuperAdminController@deleteCategory')->name('deleteCategory');

    /* Category end*/
    Route::get('manage-product','SuperAdminController@manageProduct')->name('manageproduct');
    Route::get('add-product','SuperAdminController@addProduct')->name('addproduct');
    Route::post('save-product','SuperAdminController@saveProduct')->name('saveProduct');
    Route::get('edit-product/{id}/{page}','SuperAdminController@editProduct')->name('editProduct');
    Route::get('delete-product/{id}/{page}','SuperAdminController@deleteProduct')->name('deleteProduct');
    Route::post('update-product','SuperAdminController@updateProduct')->name('updateProduct');

    /*Manufactrer start*/
    Route::get('add-manufacturer','SuperAdminController@addManufacturer')->name('addManufacturer');
    Route::post('save-manufacturer','SuperAdminController@saveManufacturer')->name('saveManufacturer');

   Route::get('manage-manufacturer','SuperAdminController@manageManufacturer')->name('manageManufacturer');
   Route::get('edit-manufacturer/{id}/{page}','SuperAdminController@editManufacturer')->name('editManufacturer');
    
    /*item controller */
    Route::get('add-item','ItemController@addItem')->name('addItem');


});