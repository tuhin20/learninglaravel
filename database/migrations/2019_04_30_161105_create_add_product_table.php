<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addProduct', function (Blueprint $table) {
            $table->bigIncrements('productId');
            $table->string('productName');
            $table->string('productImage')->nullable();
            $table->string('productDescription');
            $table->string('productPrice');
            $table->integer('categoryId');
            $table->tinyInteger('publicationStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addProduct');
    }
}
