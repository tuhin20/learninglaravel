<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddManufacturerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addmanufacturer', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('manufacturerName');
              $table->string('manufacturerDescription');
               $table->string('productId');
                $table->string('publicationStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addmanufacturer');
    }
}
